﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GFXpublisher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public List<string> bigList;

        public string incomingFolder;
        public string outputFolder;
        public string tempOutputFolder;
        public string vizRenderer_ip;
        public string vizRenderer_console_port;
        public string vizRenderer_web_api_port;
        public string ffmpeg;
        public string hostname;
        //public string concept = "";
        //public string sceneInfoLink = "";
        //public string sceneFullName = "";
        public string stretchingLength = "";
        public int checkPoint;

        public Thread WATCHFOLDER = null;
        public Thread PROCESS = null;

        private static ReaderWriterLockSlim LockFile = new ReaderWriterLockSlim();  //lock pre log file
        private static ReaderWriterLockSlim LockList = new ReaderWriterLockSlim();  //lock pre bigList
        delegate void SetTextCallback(string text);

        RestClient restclient = new RestClient()
        {
            CookieContainer = new CookieContainer()
        };

        private void SetText(string text)
        {
            if (this.richTextBox1.InvokeRequired)
            {
                var d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.richTextBox1.AppendText(text);
                this.richTextBox1.ScrollToCaret();
            }
        }

        public static string FilterWhiteSpaces(string input)
        {
            if (input == null)
                return string.Empty;

            StringBuilder stringBuilder = new StringBuilder(input.Length);
            for (int i = 0; i < input.Length; i++)
            {
                char c = input[i];
                if (i == 0 || c != ' ' || (c == ' ' && input[i - 1] != ' '))
                    stringBuilder.Append(c);
            }
            return stringBuilder.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //foreach (string s in bigList)
            //{
            //    SetText(s + "\n");
            //}
            //isLicenceValid();

            ////////string txtFile = DajPrvyZaznam();
            ////////string lines = getTextForRenderer(txtFile);

            ////////string[] sublink = lines.Split(new string[] { "&p=" }, StringSplitOptions.None);
            ////////var xdoc = new XmlDocument();
            ////////xdoc.LoadXml(sublink[1]);
            ////////getKoncept(xdoc);
        }

        protected virtual bool isFileBlocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        private void pridajDoBigListu(string zaznam)
        {
            LockList.EnterWriteLock();
            try
            {
                bigList.Add(zaznam);
            }
            finally
            {
                LockList.ExitWriteLock();
            }
        }

        private void zmazZbigListu(string item)
        {
            LockList.EnterWriteLock();
            try
            {
                bigList.Remove(item);
            }
            finally
            {
                LockList.ExitWriteLock();
            }
        }

        public bool BigListObsahuje(string zaznam)
        {
            LockList.EnterWriteLock();
            try
            {
                foreach (string s in bigList)
                {
                    if (s.Contains(zaznam))
                    {
                        return true;
                    }
                }
                return false;
            }
            finally
            {
                LockList.ExitWriteLock();
            }
        }

        private void strazApridaj()
        {
            while (true)
            {
                try
                {
                    string[] fileNames = Directory.GetFiles(incomingFolder);

                    foreach (string FileName in fileNames)
                    {
                        FileInfo fi = new FileInfo(FileName);
                        if (isFileBlocked(fi))
                        {
                            zapisLog("file " + FileName + "je locknuty (pouziva ho iny proces) - pre teraz preskakujem\n");
                        }
                        else
                        {
                            string[] parts = FileName.Split('\\');
                            string name = parts.Last();
                            string[] parts2 = name.Split('.');
                            string zaznam = parts2[0];

                            if (BigListObsahuje(zaznam))
                            {   }
                            else
                            {
                                pridajDoBigListu(incomingFolder + "\\" + zaznam + ".txt");
                            }
                        }
                    }
                    Thread.Sleep(5000);
                }
                catch (Exception ex)
                {
                    zapisLog("Vygenerovana vynimka pocas kontroly a pridavania suborov: \n" + ex.ToString());
                }
            }
        }

        private string DajPrvyZaznam()
        {
            LockList.EnterWriteLock();
            string winner = null;
            try
            {
                if (bigList.Count > 0)
                {
                    winner = bigList.First();
                    return winner;
                }
                else
                {
                    return winner;
                }
            }
            finally
            {
                LockList.ExitWriteLock();
            }
        }

        //private void getConcept(XmlNode root)
        //{
        //    if (root is XmlElement)
        //    {
        //        findConcept(root);

        //        if (root.HasChildNodes)
        //            getConcept(root.FirstChild);
        //        if (root.NextSibling != null)
        //            getConcept(root.NextSibling);
        //    }
        //    else if (root is XmlText)
        //    { }
        //    else if (root is XmlComment)
        //    { }
        //}

        //private void findConcept(XmlNode node)
        //{
        //    if (node.Attributes["name"] != null)
        //    {
        //        if (node.Name == "field" && node.Attributes["name"].Value == "concept")
        //        {
        //            concept = node.InnerText;
        //        }
        //    }
        //}

        //private void getSceneLink(XmlNode root)
        //{
        //    if (root is XmlElement)
        //    {
        //        findSceneLink(root);

        //        if (root.HasChildNodes)
        //            getSceneLink(root.FirstChild);
        //        if (root.NextSibling != null)
        //            getSceneLink(root.NextSibling);
        //    }
        //    else if (root is XmlText)
        //    { }
        //    else if (root is XmlComment)
        //    { }
        //}

        //private void findSceneLink(XmlNode node)
        //{
        //    if (node.Attributes["concept"] != null)
        //    {
        //        if (node.Name == "reflink" && node.Attributes["concept"].Value == concept)
        //        {
        //            sceneInfoLink = node.InnerText;
        //        }
        //    }
        //}

        //private void getSceneFullName(XmlNode root)
        //{
        //    if (root is XmlElement)
        //    {
        //        findSceneFullName(root);

        //        if (root.HasChildNodes)
        //            getSceneFullName(root.FirstChild);
        //        if (root.NextSibling != null)
        //            getSceneFullName(root.NextSibling);
        //    }
        //    else if (root is XmlText)
        //    { }
        //    else if (root is XmlComment)
        //    { }
        //}

        //private void findSceneFullName(XmlNode node)
        //{
        //    if (node.Name == "element" && node.Attributes["scene"].Value != null)
        //    {
        //        sceneFullName = node.Attributes["scene"].Value;
        //    }
        //}

        private void getStretchingLength(XmlNode root)
        {
            if (root is XmlElement)
            {
                StretchingLength(root);

                if (root.HasChildNodes)
                    getStretchingLength(root.FirstChild);
                if (root.NextSibling != null)
                    getStretchingLength(root.NextSibling);
            }
            else if (root is XmlText)
            { }
            else if (root is XmlComment)
            { }
        }

        private void StretchingLength(XmlNode node)
        {
            if (node.Name == "stretching" && node.Attributes["length"].Value != null)
            {
                stretchingLength = node.Attributes["length"].Value;
            }
        }

        private string sendCommandToEngine(string command)
        {
            string answer = "";
            try
            {
                TcpClient client = new TcpClient();
                client.Connect(vizRenderer_ip, Int32.Parse(vizRenderer_console_port));
                byte[] dataToServer = Encoding.UTF8.GetBytes(command);
                NetworkStream stream = client.GetStream();
                stream.Write(dataToServer, 0, dataToServer.Length);
                byte[] bytesToRead = new byte[client.ReceiveBufferSize];
                int bytesRead = stream.Read(bytesToRead, 0, client.ReceiveBufferSize);
                answer = Encoding.UTF8.GetString(bytesToRead, 0, bytesRead);
                stream.Close();
                client.Close();
            }
            catch (Exception ex)
            {
                zapisLog("Vynimka pri komunikacii s viz engine konzolou: \nCommand: " + command + "\nVynimka: " + ex.ToString() + "\n");
                answer = "exception";
            }
            return answer;
        }

        private string getHttpRequest(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                var response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader streader = new StreamReader(dataStream);
                string responseFromServer = streader.ReadToEnd();
                return responseFromServer;
            }
            catch (Exception ex)
            {
                zapisLog("Vygenerovana vynimka pri http requeste na adresu " + url + "\nvynimka: " + ex.ToString() + "\n");
                return string.Empty;
            }
        }

        private string putJsonHttpRequest(string json, string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Accept = "application/json";
                request.Method = "PUT";
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                var response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader streader = new StreamReader(dataStream);
                string responseFromServer = streader.ReadToEnd();
                return responseFromServer;
            }
            catch (Exception ex)
            {
                zapisLog("Vygenerovana vynimka pri pokuse vlozit scenu do renderera\njson: " + json + "\nurl: " + url + "\nvynimka: " + ex.ToString() + "\n");
                return string.Empty;
            }
        }

        private string getConcept(XmlDocument xml)
        {
            XmlNodeList nodeList = xml.GetElementsByTagName("field");
            foreach(XmlNode node in nodeList)
            {
                if(node.Attributes["name"].Value == "concept")
                {
                    //SetText(node.InnerText);
                    return node.InnerText;
                }
            }
            return string.Empty;
        }

        private string getSceneLink(XmlDocument xml, string concept)
        {
            XmlNodeList nodeList = xml.GetElementsByTagName("reflink");
            foreach (XmlNode node in nodeList)
            {
                if (node.Attributes["concept"].Value == concept)
                {
                    //SetText(node.InnerText);
                    return node.InnerText;
                }
            }

            return string.Empty;
        }

        private string getSceneFullName(XmlDocument xml)
        {
            XmlNodeList nodeList = xml.GetElementsByTagName("element");
            foreach (XmlNode node in nodeList)
            {
                if (node.Attributes["scene"].Value != null)
                {
                    return node.Attributes["scene"].Value;
                }
            }

            return string.Empty;
        }

        private bool VideoRender(string lines, string ID, JObject json)
        {
            try
            {
                string[] sublink = lines.Split(new string[] { "&p=" }, StringSplitOptions.None);
                var at = XElement.Parse(sublink[1]).Attribute("model");
                string link1 = at.Value;

                //zistit concept (landscape/portrait/square)
                var xdoc = new XmlDocument();
                xdoc.LoadXml(sublink[1]);
                //XmlNode root = xdoc.SelectSingleNode("*");
                string concept = getConcept(xdoc);
                zapisLog("Zistil som concept: " + concept + "\n");

                string[] slova = link1.Split('/');
                string ID1 = slova[slova.Length - 2];
                slova = link1.Split(':');
                string previewServerLink = slova[0] + ":" + slova[1] + ":8177/templates/" + ID1 + "/master_template";
                zapisLog("Vyrobeny link na preview server: " + previewServerLink + "\n");

                zapisLog("posielam request na preview server\n");
                string responseFromServer = getHttpRequest(previewServerLink);
                zapisLog("odpoved od preview servera:\n" + responseFromServer + "\n");

                xdoc.LoadXml(responseFromServer);
                //XmlNode root = xdoc.SelectSingleNode("*");
                string sceneInfoLink = getSceneLink(xdoc, concept);
                zapisLog("Link na scenu je: " + sceneInfoLink + "\n");

                zapisLog("posielam request na scenu\n");
                responseFromServer = getHttpRequest(sceneInfoLink);
                zapisLog("prijata odpoved:\n" + responseFromServer + "\n");

                xdoc.LoadXml(responseFromServer);
                //XmlNode root = xdoc.SelectSingleNode("*");
                string sceneFullName = getSceneFullName(xdoc);
                zapisLog("fullname pre scenu je: " + sceneFullName + "\n");

                //zisujem UUID sceny
                zapisLog("Komunikujem s viz engine konzolou -> send SCENE*" + sceneFullName + "*UUID GET\n");
                string answer = sendCommandToEngine("send SCENE*" + sceneFullName + "*UUID GET\0");
                if(answer == "exception")
                {
                    return false;
                }
                string uuid = answer.Split('<', '>')[1];
                zapisLog("UUID sceny z odpovedi je: " + uuid + "\n");

                //hodim scenu do renderera
                string sceneJson = "{\"Scene\": \"" + uuid + "\"}";
                string url = "http://" + vizRenderer_ip + ":" + vizRenderer_web_api_port + "/api/v1/renderer/layer/1";
                zapisLog("Idem nastavit scenu do renderera -> posielam json cez web api...\njson: " + sceneJson + "\nURL: " + url + "\n");
                responseFromServer = putJsonHttpRequest(sceneJson, url);
                zapisLog("Scena pridana do renderera\n");

                //RENDERER SET_OBJECT SCENE*Vizrt/VizStory/concept=landscape/Score
                zapisLog("Komunikujem s viz engine konzolou -> send RENDERER SET_OBJECT SCENE*" + sceneFullName + "\n");
                answer = sendCommandToEngine("send RENDERER SET_OBJECT SCENE*" + sceneFullName + "\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");

                //naplnim scenu datami 
                string[] b = lines.Split(new string[] { "&p=" }, StringSplitOptions.None);
                xdoc = new XmlDocument();
                xdoc.LoadXml(b[1]);

                List<List<string>> objekty = new List<List<string>>();
                foreach (XmlNode node in xdoc)
                {
                    foreach (XmlNode subNode in node)
                    {
                        List<string> subList = new List<string>();
                        subList.Add(subNode.Attributes["name"].Value);
                        subList.Add(subNode.InnerText);
                        objekty.Add(subList);
                    }
                }
                objekty.RemoveAt(0);

                string command = "send RENDERER*TREE*$object*FUNCTION*ControlObject*in SET ON ";
                foreach (List<string> objekt in objekty)
                {
                    command = command + objekt[0] + " SET " + objekt[1] + "\\0";
                }
                command = command + "\0";
                zapisLog("Komunikujem s viz engine konzolou -> " + command + "\n");
                answer = sendCommandToEngine(command);
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");

                // idem nastavit png renderer "send RENDER_TO_DISK*PLUGIN SET BUILT_IN*RENDER_TO_DISK*VideoRenderer"
                zapisLog("Komunikujem s viz engine konzolou -> send RENDER_TO_DISK * PLUGIN SET BUILT_IN * RENDER_TO_DISK * VideoRenderer\n");
                answer = sendCommandToEngine("send RENDER_TO_DISK*PLUGIN SET BUILT_IN*RENDER_TO_DISK*VideoRenderer\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                
                //vyber codecu RENDER_TO_DISK*PLUGIN_INSTANCE*CodecList SET 0
                zapisLog("Komunikujem s viz engine konzolou ->  RENDER_TO_DISK*PLUGIN_INSTANCE*CodecList SET 0\n");
                answer = sendCommandToEngine("send  RENDER_TO_DISK*PLUGIN_INSTANCE*CodecList SET 0\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");

                //pridanie audia do rendrovania RENDER_TO_DISK*PLUGIN_INSTANCE*RenderAudio SET 1
                zapisLog("Komunikujem s viz engine konzolou -> RENDER_TO_DISK*PLUGIN_INSTANCE*RenderAudio SET 1\n");
                answer = sendCommandToEngine("send  RENDER_TO_DISK*PLUGIN_INSTANCE*RenderAudio SET 1\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");

                //nastavenie formatu - RENDER_TO_DISK*FRAME_FORMAT SET FULL_INTERLACED_TOP
                zapisLog("Komunikujem s viz engine konzolou -> send RENDER_TO_DISK*FRAME_FORMAT SET FULL_INTERLACED_TOP\n");
                answer = sendCommandToEngine("send RENDER_TO_DISK*FRAME_FORMAT SET FULL_INTERLACED_TOP\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");

                //nastavenie formatu - RENDER_TO_DISK*PIXEL_FORMAT SET RGB
                zapisLog("Komunikujem s viz engine konzolou -> send RENDER_TO_DISK*PIXEL_FORMAT SET RGB\n");
                answer = sendCommandToEngine("send RENDER_TO_DISK*PIXEL_FORMAT SET RGB\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");

                //idem nastavit miesto ulozenia "send RENDER_TO_DISK*CLIP_NAME SET "C:/Users/traco/Desktop/testOutput/test1/pics""
                string location = tempOutputFolder + "\\" + ID + "\\video";
                zapisLog("Komunikujem s viz engine konzolou -> send RENDER_TO_DISK*CLIP_NAME SET \"" + location + "\"\n");
                answer = sendCommandToEngine("send RENDER_TO_DISK*CLIP_NAME SET \"" + location + "\"\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");

                //zistim pocet stopiek
                zapisLog("Komunikujem s viz engine konzolou -> send <" + uuid + ">*STAGE SEARCH_KEYFRAME \"*pilot*\"\n");
                answer = sendCommandToEngine("send <" + uuid + ">*STAGE SEARCH_KEYFRAME \"*pilot*\"\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                int pocet_stopiek = 0;
                foreach (char ch in answer)
                {
                    if (ch == '#')
                    {
                        pocet_stopiek++;
                    }
                }
                zapisLog("Identifikoval som " + pocet_stopiek + " stopiek v animacii\n");

                //zistujem a nastavujem stretching length
                string[] sublink2 = sublink[0].Split(new string[] { "?s=" }, StringSplitOptions.None);
                xdoc.LoadXml(sublink2[1]);
                XmlNode root = xdoc.SelectSingleNode("*");
                getStretchingLength(root);
                int stretLen = Int32.Parse(stretchingLength);
                double stretingLen = Convert.ToDouble(stretLen / 1000.00);
                int duration = Convert.ToInt32(stretingLen * 50);

                zapisLog("Komunikujem s viz engine konzolou -> send RENDER_TO_DISK*DURATION SET " + duration + "\n");
                answer = sendCommandToEngine("send RENDER_TO_DISK*DURATION SET " + duration + "\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");

                //idem nastavit resolution "send RENDER_TO_DISK*RESOLUTION SET 1920 1080"
                zapisLog("Komunikujem s viz engine konzolou -> send RENDER_TO_DISK*RESOLUTION SET 1920 1080\n");
                answer = sendCommandToEngine("send RENDER_TO_DISK*RESOLUTION SET 1920 1080\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }
                zapisLog("odpoved viz engine konzoly: " + answer + "\n");

                if (pocet_stopiek > 0)
                {
                    //zistim si id directora "send <57B3F974-E58E-214F-A0AED5F69D71E401>*STAGE GET_ALL_DIRECTORS"
                    zapisLog("Komunikujem s viz engine konzolou -> send <" + uuid + ">*STAGE GET_ALL_DIRECTORS\n");
                    answer = sendCommandToEngine("send <" + uuid + ">*STAGE GET_ALL_DIRECTORS\0");
                    if (answer == "exception")
                    {
                        zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                        return false;
                    }
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    answer = answer.Substring(5);
                    string directorID = "";
                    string pattern = @"({)|(})";
                    foreach (string result in Regex.Split(answer, pattern))
                    {
                        if (result == "{" || result == "}" || result.Length < 3)
                        { }
                        else
                        {
                            string[] ciastky = result.Split(' ');
                            if (ciastky[2] == "Director" || ciastky[2] == "Default")
                            {
                                directorID = ciastky[0];
                            }
                        }
                    }

                    if (directorID == "")
                    {
                        zapisLog("Nenasiel som spravne meno Directora, preto nie je mozne dalej pokracovat... ocakavane meno directora: \"Director\" alebo \"Default\"\n");
                        return false;
                    }

                    ////zistim dlzku sceny bez stopiek - send #directorID*DATA GET
                    zapisLog("Komunikujem s viz engine konzolou -> send " + directorID + "*DATA GET\n");
                    answer = sendCommandToEngine("send " + directorID + "*DATA GET\0");
                    if (answer == "exception")
                    {
                        zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                        return false;
                    }
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");

                    string[] kus = answer.Split(' ');
                    string dlzkaSceny = kus[kus.Length - 2];
                    double dS = Convert.ToDouble(dlzkaSceny);
                    double dlzkaStopiek = (stretingLen - dS) / Convert.ToDouble(pocet_stopiek);

                    //nastavim cas pre stopky - RENDER_TO_DISK*MAIN*STOPPAUSE SET 1.091
                    zapisLog("Komunikujem s viz engine konzolou -> send RENDER_TO_DISK*MAIN*STOPPAUSE SET " + dlzkaStopiek + "\n");
                    answer = sendCommandToEngine("send RENDER_TO_DISK*MAIN*STOPPAUSE SET " + dlzkaStopiek + "\0");
                    if (answer == "exception")
                    {
                        zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                        return false;
                    }
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                }

                //idem spustit record "send RENDER_TO_DISK RECORD INTERACTIVE 1920 1080"
                zapisLog("Komunikujem s viz engine konzolou -> send RENDER_TO_DISK RECORD INTERACTIVE 1920 1080\n");        
                answer = sendCommandToEngine("send RENDER_TO_DISK RECORD INTERACTIVE 1920 1080\0");
                if (answer == "exception")
                {
                    zapisLog("odpoved viz engine konzoly: " + answer + "\n");
                    return false;
                }

                zapisStatus(json, "RENDERING", "In Progress");
                Stopwatch time = new Stopwatch();
                time.Start();

                zapisLog("odpoved konzoly: " + answer + "\n");

                string newVideoFile = tempOutputFolder + "\\" + ID + "\\video.avi";
                FileInfo fi1 = new FileInfo(newVideoFile);
                while (isFileBlocked(fi1))
                {
                    Thread.Sleep(1000);
                }
                time.Stop();
                long durationTime = time.ElapsedMilliseconds / 1000;

                if (File.Exists(tempOutputFolder + "\\" + ID + "\\video.avi"))
                {
                    zapisLog("Rendrovanie videa trvalo: " + durationTime + " sek.\n");
                    zapisStatus(json, "RENDERING", "Done");
                    return true;
                }
                else
                {
                    zapisLog("Rendrovanie videa trvalo: " + durationTime + " sek.\n");
                    zapisStatus(json, "RENDERING", "unsuccessfully");
                    return false;
                }
            }
            catch (Exception ex)
            { 
                zapisLog("Vygenerovana vynimka vo funkcii VideoRender(): " + ex.ToString() + "\n");
                return false;
            }
        }

        private void writeStatus(JObject json, string status, string description)
        {
            int i = 1;
            while (!writeToDatabase(json, status, description) && i < 6)
            {
                zapisLog("Neuspesny pokus zapisu do databazy (" + i + "/" + "5)\n");
                i++;
                generateAccessToken();
                Thread.Sleep(500);
            }
        }

        private void zapisStatus(JObject json, string status, string description)
        {
            Thread databaseWrite = new Thread(() => writeStatus(json, status, description));
            databaseWrite.Start();
        }

        private bool transkoduj(string argument, JObject json)
        {
            zapisLog("Zaciatok transkodovania videa\n");
            checkPoint = -1;
            try
            {
                zapisStatus(json, "TRANSCODING", "Started");
                Stopwatch time = new Stopwatch();
                time.Start();
                Process p = new Process();
                p.StartInfo.FileName = ffmpeg;
                p.StartInfo.Arguments = argument;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;
                p.OutputDataReceived += (sender, args) => HandleData(json, args);
                p.ErrorDataReceived += (sender, args) => HandleData(json, args);
                p.StartInfo.CreateNoWindow = true;
                p.Start();
                p.BeginOutputReadLine();
                p.BeginErrorReadLine();
                p.WaitForExit();

                time.Stop();
                long durationTime = time.ElapsedMilliseconds / 1000;
                zapisLog("Transkodovanie videa trvalo: " + durationTime + " sek.\n");

                zapisLog("Koniec transkodovania videa\n");
                zapisStatus(json, "TRANSCODING", "Done");
                return true;
            }
            catch (Exception ex)
            {
                zapisLog("Vygenerovana vynimka pri transkodovani videa: " + ex.ToString() + "\n");
                return false;
            }
        }

        private void HandleData(JObject json, DataReceivedEventArgs outLine)
        {
            if ((outLine.Data != null) && (outLine.Data.StartsWith("frame=")))
            {
                try
                {
                    //SetText(outLine.Data + "\n");
                    double totalDuration = Convert.ToDouble(getDataFromJson(json, "duration"));

                    List<string> elementy = FilterWhiteSpaces(outLine.Data).Split(' ').ToList();
                    string time = "";

                    foreach (string s in elementy)
                    {
                        if (s.Contains("time="))
                        {
                            time = s.Split('=').Last();
                        }
                    }

                    List<string> elements = time.Split(':').ToList();
                    int hod = Convert.ToInt32(elements[0]);
                    int min = Convert.ToInt32(elements[1]);
                    double sek = Convert.ToDouble(elements[2]);
                    double aktualnyCas = TimeSpan.Parse(hod + ":" + min + ":" + sek).TotalSeconds;
                    //double aktualnyCas = convertToSeconds(hod, min, sek);   //v sekundach
                    double aktualnePercento = 100.00 * aktualnyCas / totalDuration;

                    int percento = Convert.ToInt32(aktualnePercento);
                    
                    if ((percento % 5) == 0 && checkPoint < percento)
                    {
                        zapisLog("Priebezny stav transkodovania: " + percento + "%\n");
                        zapisStatus(json, "TRANSCODING", percento + "%");
                        checkPoint = percento;
                    }
                    else
                    {

                    }
                }
                catch(Exception ex)
                {
                    zapisLog("Vygenerovana vynimka v HandleData(): " + ex.ToString() + "\n");
                }
            }
        }

        private string getTextForRenderer(string txtFile)
        {
            string response = "";
            try
            {
                foreach (string line in File.ReadAllLines(txtFile).Skip(1))
                {
                    response += line + Environment.NewLine;
                }
                return response;
            }
            catch(Exception ex)
            {
                zapisLog("Vygenerovana vynimka vo funkcii getTextForRenderer(): " + ex.ToString());
                response = "exception";
                return response;
            }
        }

        private string getDataFromJson(JObject json, string attribute)
        {
            return (string)json[attribute];
        }

        private void generateAccessToken()
        {
            try
            {
                restclient.BaseUrl = new Uri("http://" + hostname + "/api/auth/login");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("forwarded", "proto=https;host=" + hostname + "");
                request.AddHeader("accept", "application/json");
                request.AddParameter("application/json", "{ \"username\":\"Administrator\", \"password\":\"Avid123\" }", ParameterType.RequestBody);
                IRestResponse response = restclient.Execute(request);
            }
            catch(Exception ex)
            {
                zapisLog("Vygenerovana vynimka pocas generovania access tokenu: " + ex.ToString());
            }
        }

        private bool writeToDatabase(JObject json, string status, string description)
        {
            json["status"] = status;
            json["description"] = description;
            json["lastStatusUpdate"] = DateTime.Now.ToString("dd/MM/yyyy, HH:mm:ss");

            string jobID = getDataFromJson(json, "jobid");
            string uri = "http://" + hostname + "/apis/traco.acs;version=1;realm=global/setParams/gfx.publisher.jobs." + jobID;
            var jsonObject = new JObject(new JProperty("value", json));

            restclient.BaseUrl = new Uri(uri);
            var request2 = new RestRequest(Method.POST);
            request2.AddHeader("content-type", "application/json");
            request2.AddParameter("application/json", jsonObject, ParameterType.RequestBody);
            IRestResponse response2 = restclient.Execute(request2);

            if (response2.Content == "")
            {
                return true;
            }
            else
            {
                zapisLog(response2.Content + "\n");
                return false;
            }
        }

        private bool isLicenceValid()
        {
            generateAccessToken();
            restclient.BaseUrl = new Uri("http://" + hostname + "/apis/traco.acs;version=1;realm=global/getParams/license.gfx");
            var request = new RestRequest(Method.GET);
            IRestResponse response = restclient.Execute(request);
            if(response.StatusCode.ToString() == "OK")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void process()
        {
            //Renderer ren = new Renderer();
            //int ee = ren.sucet(5, 6);
            int attemptCount = 0;
            while (true)
            {
                string txtFile = DajPrvyZaznam();
                if (txtFile != null)
                {
                    try
                    {
                        if (isLicenceValid())
                        {
                            zapisLog("Uspesne overenie platnosti licencie\n");
                            string fileName = txtFile.Split('\\').Last();
                            string ID = fileName.Split('.').First();
                            string stringForRenderer = getTextForRenderer(txtFile);
                            JObject json = JObject.Parse(File.ReadAllLines(txtFile).First());
                            //SetText(ID + "\n");
                            zapisLog("Idem spracovavat ID: " + ID + "\n");

                            string responseFromServer = getHttpRequest("http://" + vizRenderer_ip + ":" + vizRenderer_web_api_port + "/api/v1/renderer/onair");
                            if (responseFromServer == "true")
                            {
                                zapisLog("Status ON AIR pre VIZ Engine je nastaveny na true\n");
                                if (File.Exists(tempOutputFolder + "\\" + ID + "\\video.avi"))
                                {
                                    zapisLog("Rendrovanie nie je mozne spustit, lebo existuje subor " + tempOutputFolder + "\\" + ID + "\\video.avi\n");
                                    File.Delete(tempOutputFolder + "\\" + ID + "\\video.avi");
                                    zapisLog("Subor " + tempOutputFolder + "\\" + ID + "\\video.avi bol zmazany\n");
                                }
                                else
                                {
                                    zapisLog("Spustam rendrovanie\n");
                                    if (VideoRender(stringForRenderer, ID, json))
                                    {
                                        zapisLog("Rendrovanie dokoncene\n");
                                        zapisStatus(json, "RENDERING", "Done");
                                        string ffmpegCommand = "-y -i " + tempOutputFolder + "\\" + ID + "\\video.avi" + " -pix_fmt yuv422p -vcodec mpeg2video -non_linear_quant 1 -flags +ildct+ilme -top 1 -dc 10 -intra_vlc 1 -qmax 3 -lmin \"1 * QP2LAMBDA\" -vtag xd5c -rc_max_vbv_use 1 -rc_min_vbv_use 1 -g 12 -b:v 50000k -minrate 50000k -maxrate 50000k -bufsize 8000k -acodec pcm_s16le -ar 48000 -bf 2 -ac 2 " + outputFolder + "\\" + ID + ".mxf";
                                        zapisLog("Spustam transkodovanie videa\n");
                                        zapisLog("prikaz pre ffmpeg: " + ffmpegCommand + "\n");
                                        if (transkoduj(ffmpegCommand, json))
                                        {
                                            zapisLog("Transkodovanie videa ukoncene\n");
                                            zmazZbigListu(txtFile);
                                            File.Move(txtFile, incomingFolder + "\\out\\" + fileName);
                                            Directory.Delete(tempOutputFolder + "\\" + ID, true);
                                            zapisLog("Priecinok " + tempOutputFolder + "\\" + ID + " uspesne vymazany\n");
                                            zapisLog("Uloha uspesne dokoncena, cakam na novu ulohu...\n");
                                            zapisStatus(json, "FINISHED", "");
                                            attemptCount = 0;
                                        }
                                        else
                                        {
                                            zapisLog("Transkodovanie videa nebolo uspesne ukoncene\n");
                                            zapisStatus(json, "ERROR", "Transcoding Failed");
                                            attemptCount++;

                                            if (attemptCount >= 3)
                                            {
                                                zmazZbigListu(txtFile);
                                                File.Move(txtFile, incomingFolder + "\\out\\" + fileName);
                                                Directory.Delete(tempOutputFolder + "\\" + ID, true);
                                                zapisLog("Priecinok " + tempOutputFolder + "\\" + ID + " uspesne vymazany\n");
                                                zapisStatus(json, "ERROR", "Request can not be processed");
                                                zapisLog("Cakam na novu ulohu...\n");
                                                attemptCount = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        zapisLog("Rendrovanie nebolo uspesne dokoncene\n");
                                        zapisStatus(json, "ERROR", "Rendering Failed");
                                        attemptCount++;

                                        if (attemptCount >= 3)
                                        {
                                            zmazZbigListu(txtFile);
                                            File.Move(txtFile, incomingFolder + "\\out\\" + fileName);
                                            if (Directory.Exists(tempOutputFolder + "\\" + ID))
                                            {
                                                Directory.Delete(tempOutputFolder + "\\" + ID, true);
                                                zapisLog("Priecinok " + tempOutputFolder + "\\" + ID + " uspesne vymazany\n");
                                            }
                                            zapisStatus(json, "ERROR", "Request can not be processed");
                                            zapisLog("Cakam na novu ulohu...\n");
                                            attemptCount = 0;
                                        }
                                    }
                                }
                            }
                            if (responseFromServer == "false")
                            {
                                zapisLog("Status ON AIR pre VIZ Engine je nastaveny na false - treba ho manualne nastavit na true (cakam 30s.)\n");
                                Thread.Sleep(30000);
                            }
                            if (responseFromServer == string.Empty)
                            {
                                zapisLog("Nepodarilo sa zistit ON AIR status pre VIZ Engine (cakam 30s.)\n");
                                Thread.Sleep(30000);
                            }
                        }
                        else
                        {
                            zapisLog("Licencia nie je platna - nemozem dalej pokracovat\n");
                            zapisStatus(JObject.Parse(File.ReadAllLines(txtFile).First()), "ERROR", "Invalid Licence");
                            zmazZbigListu(txtFile);
                            File.Move(txtFile, incomingFolder + "\\out\\" + txtFile.Split('\\').Last());
                        }
                    }
                    catch (Exception ex)
                    {
                        zapisLog("Vygenerovana vynimka v process(): " + ex.ToString() + "\n");
                    }
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(@"C:\ProgramData\GFXpublisher\settings.conf"))
            {
                foreach (string line in File.ReadAllLines(@"C:\ProgramData\GFXpublisher\settings.conf"))
                {
                    if (line.Contains("inputFolder = "))
                    {
                        string[] parts = line.Split(' ');
                        incomingFolder = parts.Last();
                        zapisLog("input folder nastaveny na: " + incomingFolder + "\n");
                    }
                    if (line.Contains("vizRenderer_ip = "))
                    {
                        string[] parts = line.Split(' ');
                        vizRenderer_ip = parts.Last();
                        zapisLog("IP VIZ Engine nastavena na: " + vizRenderer_ip + "\n");
                    }
                    if (line.Contains("vizRenderer_console_port = "))
                    {
                        string[] parts = line.Split(' ');
                        vizRenderer_console_port = parts.Last();
                        zapisLog("Port VIZ Engine konzoly nastaveny na: " + vizRenderer_console_port + "\n");
                    }
                    if (line.Contains("vizRenderer_web_api_port = "))
                    {
                        string[] parts = line.Split(' ');
                        vizRenderer_web_api_port = parts.Last();
                        zapisLog("Port VIZ Engine web api nastaveny na: " + vizRenderer_web_api_port + "\n");
                    }
                    if (line.Contains("ffmpeg.exe = "))
                    {
                        string[] parts = line.Split(' ');
                        ffmpeg = parts.Last();
                        zapisLog("ffmpeg nastaveny na: " + ffmpeg + "\n");
                    }
                    if (line.Contains("outputFolder = "))
                    {
                        string[] parts = line.Split(' ');
                        outputFolder = parts.Last();
                        zapisLog("output folder nastaveny na: " + outputFolder + "\n");
                    }
                    if (line.Contains("tempOutputFolder = "))
                    {
                        string[] parts = line.Split(' ');
                        tempOutputFolder = parts.Last();
                        zapisLog("temp output folder nastaveny na: " + tempOutputFolder + "\n");
                    }
                    if (line.Contains("hostname = "))
                    {
                        string[] parts = line.Split(' ');
                        hostname = parts.Last();
                        zapisLog("hostname nastaveny na: " + hostname + "\n");
                    }
                }

                bigList = new List<string>();
                WATCHFOLDER = new Thread(strazApridaj);
                WATCHFOLDER.Start();
                zapisLog("WATCHFOLDER THREAD uspesne spusteny\n");

                PROCESS = new Thread(process);
                PROCESS.Start();
                zapisLog("PROCESS THREAD uspesne spusteny\n");
            }
            else
            {
                zapisLog("config file (C:\\ProgramData\\GFXpublisher\\settings.conf) neexistuje");
            }
        }

        private void zapisLog(string log)
        {
            LockFile.EnterWriteLock();
            try
            {
                File.AppendAllText("log.txt", DateTime.Now.ToString() + " - " + log);
            }
            finally
            {
                LockFile.ExitWriteLock();
            }
        }     
    }
}
